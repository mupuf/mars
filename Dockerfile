FROM python:3.9

ENV DEBIAN_FRONTEND noninteractive

WORKDIR /app
COPY requirements.txt /app

RUN pip install uwsgi
RUN pip install --no-cache-dir -r /app/requirements.txt

COPY . /app

CMD uwsgi /app/uwsgi-docker.ini
