# Generated by Django 3.1.4 on 2021-03-16 15:45

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('infra', '0006_ensure_event_diff_json_is_always_type_str'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='pdu',
            name='url',
        ),
        migrations.AlterField(
            model_name='pdu',
            name='name',
            field=models.CharField(help_text='Name of the PDU', max_length=80, unique=True),
        ),
    ]
