from django.test import TestCase
from django.urls import reverse
from django.conf import settings
from datetime import datetime
import json

from .models import PDU, Machine, Event


class PDUTestCase(TestCase):
    def test_str(self):
        self.assertEqual(str(PDU(name="MyPDU")), "<PDU: MyPDU>")


class MachineTests(TestCase):
    def setUp(self):
        self.pdu = PDU.objects.create(name="MyPDU", pdu_model='PDU1')

        self.machine = Machine.objects.create(base_name="gfx8",
                                              mac_address="de:ad:be:ef:ca:fe",
                                              ip_address="192.168.0.1",
                                              pdu=self.pdu, pdu_port_id="42")

    def test_full_name__on_unsaved_object(self):
        with self.assertRaises(ValueError):
            Machine().full_name

    def test_full_name(self):
        # Check the name of the fixture machine
        self.assertEqual(self.machine.full_name, f"{settings.FARM_NAME}-gfx8-1")

        # Make sure that the index is not affected by other basenames, and
        # increases nicely
        for i in range(1, 9):
            m = Machine.objects.create(base_name="gfx9",
                                       mac_address=f"de:ad:be:ef:ca:0{i}",
                                       ip_address="192.168.0.1")
            self.assertEqual(m.full_name, f"{settings.FARM_NAME}-gfx9-{i}")

        # Check that the FARM_NAME setting is honored
        with self.settings(FARM_NAME='tchar'):
            # Due to full_name being cached_property, create another machine to pick up the setting change.
            another_machine = Machine.objects.create(base_name="gfx10",
                                                     mac_address="de:ad:be:ef:ca:ef",
                                                     ip_address="192.168.0.2",
                                                     pdu=self.pdu, pdu_port_id="43")
            # Check the name of the fixture machine
            self.assertEqual(another_machine.full_name, "tchar-gfx10-1")

    def test_filter_ready_machine(self):
        for i in range(2, 10):
            m = Machine.objects.create(base_name="gfx9",
                                       mac_address=f"de:ad:be:ef:ca:0{i}",
                                       ip_address="192.168.0.1")
            if i in [2, 4, 6]:
                m.ready_for_service = True
                m.save()

        url = reverse('machine-list')
        response = self.client.get(url + '?ready_for_service=True')
        self.assertEqual(len(response.json()), 3)

    def test_str(self):
        self.assertEqual(str(self.machine), f"<Machine: {self.machine.full_name}>")


def _get_events(**filters):
    # These are sorted by reverse normally, so reverse it once again
    return Event.objects.filter(**filters).order_by('date')


class EventTests(TestCase):
    def setUp(self):
        self.machine = Machine.objects.create(base_name="gfx8",
                                              mac_address="de:ad:be:ef:ca:fe",
                                              ip_address="192.168.0.1")

    def test_event_machine_created(self):
        events = _get_events(machine=self.machine)
        self.assertEqual(events.count(), 1)
        event = events[0]
        self.assertEqual(str(event),
                         f"<Event id=1 machine=<Machine: {settings.FARM_NAME}-gfx8-1> category={Event.CATEGORY_MACHINE_CREATED}>")  # noqa: E501
        self.assertEqual(json.loads(event.diff),
                         {})

    def test_event_machine_updated(self):
        self.machine.save()
        events = _get_events(machine=self.machine)
        self.assertEqual(events.count(), 2)
        update_event = events[1]
        self.assertEqual(str(update_event),
                         f"<Event id=2 machine=<Machine: {settings.FARM_NAME}-gfx8-1> category={Event.CATEGORY_MACHINE_UPDATED}>")  # noqa: E501
        self.assertEqual(json.loads(update_event.diff),
                         {})

    def test_event_machine_retired(self):
        self.machine.is_retired = True
        self.machine.save()
        events = _get_events(machine=self.machine)
        self.assertEqual(events.count(), 2)
        retire_event = events[1]
        self.assertEqual(str(retire_event),
                         f"<Event id=2 machine=<Machine: {settings.FARM_NAME}-gfx8-1> category={Event.CATEGORY_MACHINE_RETIRED}>")  # noqa: E501
        assert {
            'values_changed': {
                'root.is_retired': {
                    'new_value': True,
                    'old_value': False
                }
            }
        } == json.loads(retire_event.diff)

    def test_event_date_filter(self):
        now = datetime.now()
        self.machine.save()
        self.machine.save()
        url = reverse('event-list')
        response = self.client.get(url + f'?since={now}')
        self.assertEqual(len(response.json()), 2)

    def test_nullable_field_created(self):
        pdu = PDU(name='PDU1', pdu_model="dummy", config={"ports": ["label 1", "label 2"]})
        pdu.save()
        self.machine.pdu = pdu
        self.machine.pdu_port_id = 'label 1'
        now = datetime.now()
        self.machine.save()
        url = reverse('event-list')
        response = self.client.get(url + f'?since={now}')
        events = response.json()
        assert len(events) == 1
        assert {"type_changes": {
            "root.pdu_id": {
                "old_type": "NoneType",
                "new_type": "int",
                "old_value": None,
                "new_value": 1
            },
            "root.pdu_port_id": {
                "old_type": "NoneType",
                "new_type": "str",
                "old_value": None,
                "new_value": "label 1"
            }
        }} == json.loads(events[0]['diff'])
