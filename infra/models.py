from django.db import models
from django.core.validators import RegexValidator, MinValueValidator, MaxValueValidator
from django.utils.functional import cached_property
from django.utils import timezone
from django.conf import settings


class PDU(models.Model):
    name = models.CharField(unique=True, help_text="Name of the PDU", max_length=80)

    pdu_model = models.CharField(max_length=80, help_text="Model of the PDU you are using")
    config = models.JSONField(blank=True, default=dict,
                              help_text="Opaque configuration needed to instanciate the PDU")

    def __str__(self):
        return f"<PDU: {self.name}>"


class Machine(models.Model):
    # Naming and tagging
    base_name = models.CharField(max_length=50,
                                 help_text="Non-unique name that will form the basis for the full name")
    tags = models.JSONField(blank=True, null=True, default=list,
                            help_text="List of tags that the machine has")

    # Network
    mac_address = \
        models.CharField(max_length=17, unique=True, blank=False, null=False,
                         validators=[
                             RegexValidator(
                                regex='^([0-9A-Fa-f]{2}:){5}([0-9A-Fa-f]{2})$', code='invalid_format',
                                message='Format: 60:6d:3c:51:c4:88',
                                )])
    ip_address = models.GenericIPAddressField(blank=False, null=False)

    local_tty_device = \
        models.CharField(max_length=16, blank=True, null=True,
                         help_text=("Machine-local TTY device that can be used as a serial console. "
                                    "See the `console=` format at "
                                    "https://www.kernel.org/doc/html/latest/admin-guide/serial-console.html"))

    # PDU
    pdu = models.ForeignKey(PDU, null=True, blank=True, on_delete=models.SET_NULL,
                            help_text="PDU to which this machine is connected to")
    pdu_port_id = models.CharField(max_length=80, blank=True, null=True,
                                   help_text="ID of the port to which this machine is connected to")
    pdu_off_delay = models.PositiveSmallIntegerField(default=5,
                                                     validators=[
                                                        MaxValueValidator(100),
                                                        MinValueValidator(1)
                                                     ],
                                                     help_text="Number of seconds to wait in a ON->OFF->ON transition")

    # Flags
    ready_for_service = models.BooleanField(blank=False, null=False, default=False,
                                            help_text="Is the machine fit for service?")
    is_retired = models.BooleanField(blank=False, null=False, default=False,
                                     help_text="Has the machine been retired?")

    # Some statistics
    first_seen = models.DateTimeField(auto_now_add=True,
                                      help_text='When this MAC address was first seen')
    last_updated = models.DateTimeField(auto_now=True,
                                        help_text='When this machine was last modified')

    class Meta:
        ordering = ['-id']

    @cached_property
    def full_name(self):
        if self.pk is None:
            raise ValueError("The Machine needs to be saved before a name can be generated")

        idx = Machine.objects.filter(base_name=self.base_name, id__lte=self.id).count()
        return f"{settings.FARM_NAME}-{self.base_name}-{idx}"

    def __str__(self):
        return f"<Machine: {self.full_name}>"


class Event(models.Model):
    machine = models.ForeignKey(
        Machine,
        on_delete=models.CASCADE,
        help_text='The machine this event originated from.')

    CATEGORY_MACHINE_CREATED = 'machine-created'
    CATEGORY_MACHINE_UPDATED = 'machine-updated'
    CATEGORY_MACHINE_RETIRED = 'machine-retired'

    CATEGORY_CHOICES = (
        (CATEGORY_MACHINE_CREATED, 'Machine has been created'),
        (CATEGORY_MACHINE_UPDATED, 'Machine has been updated'),
        (CATEGORY_MACHINE_RETIRED, 'Machine has been retired'),
    )

    category = models.CharField(
        max_length=25,
        choices=CATEGORY_CHOICES,
        help_text='The category of the event.')
    date = models.DateTimeField(
        default=timezone.now,
        help_text='The time this event was created.')

    diff = models.JSONField(blank=True, null=True,
                            help_text="Dictionary of differences before and after the machine changed")

    def __str__(self):
        return f"<Event id={self.id} machine={self.machine} category={self.category}>"

    class Meta:
        ordering = ['-date']
