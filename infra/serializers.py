from rest_framework import serializers
from .models import PDU, Machine, Event


class MachineSerializer(serializers.HyperlinkedModelSerializer):
    url = serializers.HyperlinkedRelatedField(read_only=True, view_name='machine-detail', lookup_field='mac_address')

    full_name = serializers.ReadOnlyField()

    class Meta:
        model = Machine
        exclude = []
        read_only_fields = ['first_seen', 'last_updated']


class PDUSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = PDU
        exclude = []


class EventSerializer(serializers.ModelSerializer):
    machine = serializers.HyperlinkedRelatedField(read_only=True,
                                                  view_name='machine-detail',
                                                  lookup_field='mac_address')

    class Meta:
        model = Event
        exclude = []
